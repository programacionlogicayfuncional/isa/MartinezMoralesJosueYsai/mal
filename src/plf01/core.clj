(ns plf01.core
  (:gen-class))

(defn funcion<-1 
  [x y]
  (< x y))

(defn funcion<-2 
  [x y z]
  (< x y z))

(defn funcion<-3 
  [w x y z]
  (< w x y z))

(funcion<-1 20 40)
(funcion<-2 60 40 50)
(funcion<-3 3 6 7 8)

(defn funcion<=-1 
  [x y]
  (<= x y))

(defn funcion<=-2 
  [x y z]
  (<= x y z))

(defn funcion<=-3 
  [w x y z]
  (<= w x y z))

(funcion<=-1 45 45)
(funcion<=-2 60 80 100)
(funcion<=-3 10 6 7 8)

(defn funcion==-1 
  [x y]
  (== x y))

(defn funcion==-2 
  [x y z]
  (== x y z))

(defn funcion==-3 
  [w x y z]
  (== w x y z))

(funcion==-1 1.0 1)
(funcion==-2 20 20 40)
(funcion==-3 3 4 5 6)

(defn funcion>-1
  [x y]
  (> x y))

(defn funcion>-2
  [x y z]
  (> x y z))

(defn funcion>-3
  [w x y z]
  (> w x y z))

(funcion>-1 10 5)
(funcion>-2 20 40 60)
(funcion>-3 6 7 2 8)

(defn funcion>=-1
  [x y]
  (>= x y))

(defn funcion>=-2
  [x y z]
  (>= x y z))

(defn funcion>=-3
  [w x y z]
  (>= w x y z))

(funcion>=-1 50 10)
(funcion>=-2 100 200 300)
(funcion>=-3 6 7 2 8)

(defn funcion-assoc-1
  [mapa x y]
  (assoc mapa x y))

(defn funcion-assoc-2
  [vector x y]
  (assoc vector x y))

(defn funcion-assoc-3
  [x y]
  (assoc [50 60 70] x y))

(funcion-assoc-1 {5 6 7 8} 4 100)
(funcion-assoc-2 [20 25 30 50 55 60 65] 6 200)
(funcion-assoc-3 1 500)
 
(defn funcion-assoc-in-1
  [vector x y]
  (assoc-in vector x y))

(defn funcion-assoc-in-2
  [mapa x y]
  (assoc-in mapa x y))

(defn funcion-assoc-in-3
  [x y z]
  (assoc-in [[1 1 1]
             [1 1 1]
             [1 1 1]] [x y] z))

(funcion-assoc-in-1  [{:name "James" :age 26} {:name "John" :age 43}] [1 :age] 44)
(funcion-assoc-in-2 {:user {:bar "baz"}} [:user :id] "asd" )
(funcion-assoc-in-3 1 1 0)

(defn funcion-concat-1
  [x y]
  (concat x y))

(defn funcion-concat-2
  [vector vector2 lista mapa]
  (concat vector vector2 lista mapa))

(defn funcion-concat-3
  [x y]
  (into [] (concat x y)))

(funcion-concat-1 "abc" "def")
(funcion-concat-2 [1 2 3] [1 1 2] '(3 4) #{9 10 8})
(funcion-concat-3 [1 5 9] [50 60])

(defn funcion-conj-1
  [a b c d x]
  (conj [a b c d] x))

(defn funcion-conj-2
  [x y]
  (conj x y))

(defn funcion-conj-3
  [a b c]
  (conj a b c))

(funcion-conj-1 "a" "b" "c" "d" "e")
(funcion-conj-2 #{1 2} #{3})
(funcion-conj-3 {:a 1} {:b 2 :c 3} {:d 4 :e 5 :f 6})

(defn funcion-cons-1
  [x lista]
  (cons x lista))

(defn funcion-cons-2
  [vector vector2]
  (cons vector vector2))

(defn funcion-cons-3
  [x]
  (cons x nil))

(funcion-cons-1 50 '(60 70 80 90))
(funcion-cons-2 [1 2] [4 5 6])
(funcion-cons-3 5) 

(defn funcion-contains?-1
  [mapa x]
  (contains? mapa x))

(defn funcion-contains?-2
  [mapa x]
  (contains? mapa x))

(defn funcion-contains?-3
  [vector x]
  (contains? vector x))

(funcion-contains?-1 #{"a" "b" "c"} "a")
(funcion-contains?-2 {:a 1 :b 2 :c 3} :a)
(funcion-contains?-3 [:a] :a)

(defn funcion-count-1
  [vector]
  (count vector))

(defn funcion-count-2
  [lista x]
  (= (count lista) x))

(defn funcion-count-3
  [x]
  (count x))

(funcion-count-1 [50 60 80 1 2 5 15])
(funcion-count-2 '(15 16 21 23 28) 5)
(funcion-count-3 "parangaricutirimicuaro")

(defn funcion-disj-1
  [conjunto x y z]
  (disj conjunto x y z))

(defn funcion-disj-2
  [conjunto x]
  (disj conjunto x))

(defn funcion-disj-3
  [x]
  (disj #{[1 2 3]
          [4 5 6]
          [7 8 9]} x))

(funcion-disj-1 #{50 40 2 13 89} 2 50 13)
(funcion-disj-2 #{"asd" \a :a 15} 2)
(funcion-disj-3 [4 5 6])

(defn funcion-dissoc-1
  [mapa x]
  (dissoc mapa x))

(defn funcion-dissoc-2
  [mapa x]
  (dissoc mapa x))

(defn funcion-dissoc-3
  [x]
  (dissoc {[1 2 3]
          [4 5 6]
          [7 8 9]
          [10 11 12]} x))

(funcion-dissoc-1 {:a 12 :b 50 :c 80} :a)
(funcion-dissoc-2 {:name "Estefania" :age 23} :age)
(funcion-dissoc-3 [7 8 9])

(defn funcion-distinct-1
  [vector]
  (distinct vector))

(defn funcion-distinct-2
  [vector]
  (distinct vector))

(defn funcion-distinct-3
  [vector vector2 vector3 vector4]
  (distinct [vector vector2 vector3 vector4]))

(funcion-distinct-1 [10 5 3 8 5 5 10])
(funcion-distinct-2 ["Hola" "Adios" "Como Estas" "Hola"])
(funcion-distinct-3 [1 2 3]
                    [4 5 6]
                    [7 8 9]
                    [1 2 3])

(defn funcion-distinct?-1
  [x y z w]
  (distinct? x y z w))

(defn funcion-distinct?-2
  [x y z]
  (distinct? x y z))

(defn funcion-distinct?-3
  [vector vector2 vector3 vector4]
  (distinct? vector vector2 vector3 vector4))

(funcion-distinct?-1 1 2 3 3)
(funcion-distinct?-2 "Hola" "Adios" "Hola")
(funcion-distinct?-3 [1 2 3]
                     [4 5 6]
                     [7 8 9]
                     [1 2 3])

(defn funcion-drop-last-1
  [vector]
  (drop-last vector))

(defn funcion-drop-last-2
  [vector x]
  (drop-last x vector))

(defn funcion-drop-last-3
  [lista x]
  (drop-last x lista))

(funcion-drop-last-1 [10 51 65 37 85 15])
(funcion-drop-last-2 [84 66 93 17 82 22] 3)
(funcion-drop-last-3 '([10 25] [85 65] [15 32] [89 32]) 2)

(defn funcion-empty-1
  [vector]
  (empty vector))

(defn funcion-empty-2
  [vector]
  (map empty vector))

(defn funcion-empty-3
  [x]
  (empty x))

(funcion-empty-1 [15 98 12 85 65 37 94])
(funcion-empty-2 [[\a \b] {1 2} (range 4)])
(funcion-empty-3 "Hola")

(defn funcion-empty?-1
  [vector]
  (empty? vector))

(defn funcion-empty?-2
  [vector]
  (map empty? vector))

(defn funcion-empty?-3
  [x]
  (empty? x))

(funcion-empty?-1 [15 98 12 85 65 37 94])
(funcion-empty?-2 [[\a \b] {} (range 4)])
(funcion-empty?-3 "Hola")

(defn funcion-even?-1
  [x]
  (even? x))

(defn funcion-even?-2
  [x]
  (filter even? x))

(defn funcion-even?-3
  [x]
  (even? x))

(funcion-even?-1 10)
(funcion-even?-2 (range 100))
(funcion-even?-3 3)

(defn funcion-false?-1
  [x]
  (false? x))

(defn funcion-false?-2
  [vector]
  (map false? vector))

(defn funcion-false?-3
  [x]
  (false? x))

(funcion-false?-1 true)
(funcion-false?-2 [false true false])
(funcion-false?-3 "foo")

(defn funcion-find-1
  [mapa x]
  (find mapa x))

(defn funcion-find-2
  [vector x]
  (find vector x))

(defn funcion-find-3
  [map x]
  (find map x))

(funcion-find-1 {:a 10 :b 20 :c 30} :b)
(funcion-find-2 [:a :b :C :d] 2)
(funcion-find-3 {:a :b :c 5 8 9} 7)

(defn funcion-first-1
  [lista]
  (first lista))

(defn funcion-first-2
  [vector]
  (first vector))

(defn funcion-first-3
  [map]
  (first map))

(funcion-first-1 '(:alpha :bravo :charlie))
(funcion-first-2  [[1 2 3]
                   [4 5 6]
                   [7 8 9]
                   [1 2 3]])
(funcion-first-3 {:a :b :c 5 8 9})

(defn funcion-flatten-1
  [lista]
  (flatten lista))

(defn funcion-flatten-2
  [vector]
  (flatten vector))

(defn funcion-flatten-3
  [vector]
  (flatten vector))

(funcion-flatten-1 '(:alpha :bravo :charlie [5 2 (8 9)]))
(funcion-flatten-2  [[1 2 3]
                   [4 5 6]
                   [7 8 9]
                   [1 2 3]])
(funcion-flatten-3 [{:a :b :c 5 8 9} {:a "Josue"} {:b "Estefania"}{:c["Buenas" "Tardes"]}])

(defn funcion-frequencies-1
  [lista]
  (frequencies lista))

(defn funcion-frequencies-2
  [vector]
  (frequencies vector))

(defn funcion-frequencies-3
  [vector]
  (frequencies vector))

(funcion-frequencies-1 '(a b c d a c c))
(funcion-frequencies-2  [[1 2 3]
                         [4 5 6]
                         [7 8 9]
                         [1 2 3]])
(funcion-frequencies-3 '(:a :b :c 5 8 9 :a :c :c 5 5 9 1 8))

(defn funcion-get-1
  [mapa x]
  (get mapa x))

(defn funcion-get-2
  [vector x]
  (get vector x))

(defn funcion-get-3
  [lista x]
  (get lista x))

(funcion-get-1 {"Hola" "Como" "Estas" "UwU" } "Estas")
(funcion-get-2  [50 65 89 32 18 35] 4)
(funcion-get-3 {:a :b :c 5 8 9} :c)

(defn funcion-get-in-1
  [vector x]
  (get-in vector x))

(defn funcion-get-in-2
  [mapa x]
  (get-in mapa x))

(defn funcion-get-in-3
  [map x]
  (get-in map x))

(funcion-get-in-1 [[1 2 3]
                   [4 5 6]
                   [7 8 9]
                   [1 2 3]] [0 2])
(funcion-get-in-2  {:ursname "josue"
                    :pets [{:name "Syndra"
                            :type :dog}
                           {:name "Rayo"
                            :type :cat}]} [:pets 1 :type])
(funcion-get-in-3 {:a [{:b1 2} {:b2 4}] :c 3} [:a 0 :b1])

(defn funcion-into-1
  [vector]
  (into () vector))

(defn funcion-into-2
  [mapa]
  (into () mapa))

(defn funcion-into-3
  [vector lista]
  (into vector lista))

(funcion-into-1 [[1 2 3]
                 [4 5 6]
                 [7 8 9]
                 [1 2 3]])
(funcion-into-2  {:ursname "josue"
                  :pets [{:name "Syndra"
                          :type :dog}
                         {:name "Rayo"
                          :type :cat}]})
(funcion-into-3 [5 85 121] '(15 32 84))

(defn funcion-key-1
  [mapa]
  (map key mapa))

(defn funcion-key-2
  [mapa]
  (map key mapa))

(defn funcion-key-3
  [mapa]
  (map key mapa))

(funcion-key-1 {[:a 1 :b 2 :c 3]
                [:d 4 :e 5 :f 6]
                [:g 7 :h 8 :i 9]
                [:j 10 :k 11 :l 12]})
(funcion-key-2  {:ursname "josue"
                  :pets [{:name "Syndra"
                          :type :dog}
                         {:name "Rayo"
                          :type :cat}]})
(funcion-key-3 {:a 5 :b 85 :c 121})

(defn funcion-keys-1
  [mapa]
  (keys mapa))

(defn funcion-keys-2
  [mapa]
  (keys mapa))

(defn funcion-keys-3
  [mapa]
  (keys mapa))

(funcion-keys-1 {[:a 1 :b 2 :c 3]
                [:d 4 :e 5 :f 6]
                [:g 7 :h 8 :i 9]
                [:j 10 :k 11 :l 12]})
(funcion-keys-2  {:ursname "josue"
                 :pets [{:name "Syndra"
                         :type :dog}
                        {:name "Rayo"
                         :type :cat}]})
(funcion-keys-3 {:a 5 :b 85 :c 121})

(defn funcion-max-1
  [lista]
  (apply max lista))

(defn funcion-max-2
  [vector]
  (apply max vector))

(defn funcion-max-3
  [mapa]
  (apply max mapa))

(funcion-max-1 '(1 2 3
                 4 5 6
                 7 8 9
                 10 11 12))
(funcion-max-2 [51 20 88 208])
(funcion-max-3 [5 85 121])

(defn funcion-merge-1
  [mapa mapa2]
  (merge mapa mapa2))

(defn funcion-merge-2
  [mapa mapa1]
  (merge mapa mapa1))

(defn funcion-merge-3
  [mapa mapa1 mapa2]
  (merge mapa mapa1 mapa2))

(funcion-merge-1 {:a 1 :b 2 :c 3
                  :d 4 :e 5 :f 6
                  :g 7 :h 8}
                 {:i 10 :j 11})
(funcion-merge-2 {:a [51 20 88 208]} {:b [54 21 89 123]})
(funcion-merge-3 {:a [5 85 121]} {:b [6 95 123 84 32]} {:c [51 8964 21 54 21]})

(defn funcion-min-1
  [lista]
  (apply min lista))

(defn funcion-min-2
  [vector]
  (apply min vector))

(defn funcion-min-3
  [mapa]
  (apply min mapa))

(funcion-min-1 '(1 2 3
                   4 5 6
                   7 8 9
                   10 11 12))
(funcion-min-2 [51 20 88 208])
(funcion-min-3 [5 85 121])

(defn funcion-neg?-1
  [x]
  (neg? x))

(defn funcion-neg?-2
  [vector]
  (map neg? vector))

(defn funcion-neg?-3
  [mapa]
  (map neg? mapa))

(funcion-neg?-1 -100)
(funcion-neg?-2 [-51 -20 88 208])
(funcion-neg?-3 '(85 121 -5641 -56 -15 -5))

(defn funcion-nil?-1
  [x]
  (nil? x))

(defn funcion-nil?-2
  [vector]
  (nil? vector))

(defn funcion-nil?-3
  [mapa]
  (map nil? mapa))

(funcion-nil?-1 nil)
(funcion-nil?-2 [])
(funcion-nil?-3 '([]{}))

(defn funcion-not-empty-1
  [vector]
  (not-empty vector))

(defn funcion-not-empty-2
  [mapa]
  (not-empty mapa))

(defn funcion-not-empty-3
  [lista]
  (map not-empty lista))

(funcion-not-empty-1 [1 65 21]) 
(funcion-not-empty-2 {})
(funcion-not-empty-3 '([] {} #{}))

(defn funcion-nth-1
  [vector x]
  (nth vector x))

(defn funcion-nth-2
  [lista x]
  (nth lista x))

(defn funcion-nth-3
  [mapa x]
  (nth mapa x))

(funcion-nth-1 [[15 65 32] [12 12] [15 321 89 23]] 2)
(funcion-nth-2 '({21 87 25 86}{1123 64 2138 72}) 0)
(funcion-nth-3 [132 8 123 "a" "b" "c" "d" [21 21]] 5)

(defn funcion-odd?-1
  [vector]
  (map odd? vector))

(defn funcion-odd?-2
  [x]
  (odd? x))

(defn funcion-odd?-3
  [x]
  (filter odd? x))

(funcion-odd?-1 [20 65 28 65])
(funcion-odd?-2 120)
(funcion-odd?-3 (range 10))

(defn funcion-partition-1
  [x y]
  (partition x y))

(defn funcion-partition-2
  [x y z]
  (partition x y z))

(defn funcion-partition-3
  [w x y z]
  (partition w x y z))

(funcion-partition-1 4 (range 50))
(funcion-partition-2 5 8 (range 100))
(funcion-partition-3 3 6 ["a" "b" "c" "d"] (range 10))

(defn funcion-partition-all-1
  [x y]
  (partition-all x y))

(defn funcion-partition-all-2
  [x y z]
  (partition-all x y z))

(defn funcion-partition-all-3
  [w x]
  (partition-all w x))

(funcion-partition-all-1 4 [51 65 98 12 32 54])
(funcion-partition-all-2 2 4 [4 54 15 58 1 32 68 48 665 51 65 12])
(funcion-partition-all-3 2 "asdasdasd")

(defn funcion-peek-1
  [vector]
  (peek vector))

(defn funcion-peek-2
  [lista]
  (peek lista))

(defn funcion-peek-3
  [vector]
  (peek vector))

(funcion-peek-1 [51 65 98 12 32 54])
(funcion-peek-2 '(4 54 15 58 1 32 68 48 665 51 65 12))
(funcion-peek-3 [])

(defn funcion-pop-1
  [vector]
  (pop vector))

(defn funcion-pop-2
  [lista]
  (pop lista))

(defn funcion-pop-3
  [vector]
  (map pop vector))

(funcion-pop-1 [51 65 98 12 32 54])
(funcion-pop-2 '(4 54 15 58 1 32 68 48 665 51 65 12))
(funcion-pop-3 [[52 51 68] '(51 87 32 15)])

(defn funcion-pos?-1
  [x]
  (pos? x))

(defn funcion-pos?-2
  [vector]
  (map pos? vector))

(defn funcion-pos?-3
  [lista]
  (map pos? lista))

(funcion-pos?-1 0)
(funcion-pos?-2 [-51 -20 88 208])
(funcion-pos?-3 '(85 121 -5641 -56 -15 -5))

(defn funcion-quot-1
  [x y]
  (quot x y))

(defn funcion-quot-2
  [x y]
  (quot x y))

(defn funcion-quot-3
  [x y]
  (quot x y))

(funcion-quot-1 100 25)
(funcion-quot-2 -40 2)
(funcion-quot-3 20/2 4)

(defn funcion-range-1
  [x]
  (take x (range)))

(defn funcion-range-2
  [x]
  (range x))

(defn funcion-range-3
  [x y]
  (range x y))

(funcion-range-1 10)
(funcion-range-2 20)
(funcion-range-3 10 20)

(defn funcion-rem-1
  [x y]
  (rem x y))

(defn funcion-rem-2
  [x y]
  (rem x y))

(defn funcion-rem-3
  [x y]
  (rem x y))

(funcion-rem-1 100 30)
(funcion-rem-2 -40 3)
(funcion-rem-3 20/2 4)

(defn funcion-repeat-1
  [x y]
  (take x (repeat y)))

(defn funcion-repeat-2
  [x y]
  (repeat x y))

(defn funcion-repeat-3
  [x y]
  (repeat x y))

(funcion-repeat-1 5 "x")
(funcion-repeat-2 5 "hola")
(funcion-repeat-3 5 [21 84 68])

(defn funcion-replace-1
  [x y z]
  (clojure.string/replace x y z))

(defn funcion-replace-2
  [x y z]
  (clojure.string/replace x y z))

(defn funcion-replace-3
  [x y z]
  (clojure.string/replace x y z))

(funcion-replace-1 "Mi perro es azul" #"azul" "rojo")
(funcion-replace-2 "mi perro es azul" #"\b." #(.toUpperCase %1))
(funcion-replace-3 "Mi gato es rojo" #"[aeiou]" #(str %1 %1))

(defn funcion-rest-1
  [vector]
  (rest vector))

(defn funcion-rest-2
  [vector]
  (rest vector))

(defn funcion-rest-3
  [lista]
  (rest lista))

(funcion-rest-1 [51 68 12 35])
(funcion-rest-2 [[-51 -20] [65 89]])
(funcion-rest-3 '(85 121 -5641 -56 -15 -5))

(defn funcion-select-keys-1
  [mapa x]
  (select-keys mapa x))

(defn funcion-select-keys-2
  [vector x]
  (select-keys vector x))

(defn funcion-select-keys-3
  [lista x]
  (select-keys lista x))

(funcion-select-keys-1 {:a 51 :b 68 :c 12 :d 35} [:c])
(funcion-select-keys-2 [:A [-51 -20] :B [65 89]] [2 1])
(funcion-select-keys-3 [:a 85 :b 121 :c 5641 :d -56 :f -15 :g -5] [3 4])

(defn funcion-shuffle-1
  [vector]
  (shuffle vector))

(defn funcion-shuffle-2
  [lista]
  (shuffle lista))

(defn funcion-shuffle-3
  [lista x]
  (repeatedly x (partial shuffle lista)))

(funcion-shuffle-1 [51 38 65 120])
(funcion-shuffle-2 '([-51 -20] [65 89] [554 89]))
(funcion-shuffle-3 '(1 2 3 4 5 6) 5)

(defn funcion-sort-1
  [vector]
  (sort vector))

(defn funcion-sort-2
  [lista]
  (sort lista))

(defn funcion-sort-3
  [lista]
  (sort lista))

(funcion-sort-1 [51 38 65 120])
(funcion-sort-2 '([-51 -20] [65 89] [554 89]))
(funcion-sort-3 '(1 2 3 4 5 6))

(defn funcion-split-at-1
  [vector x]
  (split-at x vector))

(defn funcion-split-at-2
  [lista x]
  (split-at x lista))

(defn funcion-split-at-3
  [lista x]
  (split-at x lista))

(funcion-split-at-1 [51 38 65 120] 3)
(funcion-split-at-2 '([-51 -20] [65 89] [554 89]) 2)
(funcion-split-at-3 '(1 2 3 4 5 6) 5)

(defn funcion-str-1
  [vector vector1]
  (apply str vector vector1))

(defn funcion-str-2
  [lista]
  (str lista))

(defn funcion-str-3
  [lista]
  (apply str lista))

(funcion-str-1 [[51 38 65 120] [21 98 13]] [[21 51] [51 32]])
(funcion-str-2 '("Mi perro" "es" "azul"))
(funcion-str-3 '(:a :b :c :d :e))

(defn funcion-subs-1
  [cadena x y]
  (subs cadena x y))

(defn funcion-subs-2
  [cadena x]
  (subs cadena x))

(defn funcion-subs-3
  [cadena x]
  (subs cadena x))

(funcion-subs-1 "Mi perro es azul" 4 8)
(funcion-subs-2  "Hola como estas"2)
(funcion-subs-3 "1 2 3 4 5 6 7 8 9 10" 2)

(defn funcion-subvec-1
  [vector x]
  (subvec vector x))

(defn funcion-subvec-2
  [vector x y]
  (subvec vector x y))

(defn funcion-subvec-3
  [vector x]
  (subvec vector x))

(funcion-subvec-1 [51 89 32 12] 2)
(funcion-subvec-2  [12 32 65 78 41 63] 2 5)
(funcion-subvec-3 [[32 98] [153 2] [12 15]] 2)


(defn funcion-take-1
  [vector x]
  (take x vector))

(defn funcion-take-2
  [lista x]
  (take x lista))

(defn funcion-take-3
  [vector x]
  (take vector x))

(funcion-take-1 3 [51 89 32 12])
(funcion-take-2  '(12 32 65 78) 3)
(funcion-take-3 2 [[32 98] [153 2] [12 15]])

(defn funcion-true?-1
  [x]
  (true? x))

(defn funcion-true?-2
  [vector]
  (map true? vector))

(defn funcion-true?-3
  [x]
  (true? x))

(funcion-true?-1 -100)
(funcion-true?-2 [true -20 true 208])
(funcion-true?-3 (= 1 1))

(defn funcion-val-1
  [mapa]
  (val (first mapa)))

(defn funcion-val-2
  [mapa]
  (map val mapa))

(defn funcion-val-3
  [mapa]
  (val (last mapa)))

(funcion-val-1 {:a 15 :b 12 :c 18 :d 88})
(funcion-val-2 {:a [15 98 12] :b [12 1] :c [98 56]})
(funcion-val-3 {:a [321 1] :b [12 15] :c [51 87]})

(defn funcion-vals-1
  [mapa]
  (vals mapa))

(defn funcion-vals-2
  [mapa]
  (vals mapa))

(defn funcion-vals-3
  [mapa]
  (vals mapa))

(funcion-vals-1 {:a 15 :b 12 :c 18 :d 88})
(funcion-vals-2 {:a {15 98} :b {12 1} :c {98 56}})
(funcion-vals-3 {:a [321 1] :b [12 15] :c [51 87]})

(defn funcion-zero?-1
  [x]
  (zero? x))

(defn funcion-zero?-2
  [vector]
  (map zero? vector))

(defn funcion-zero?-3
  [lista]
  (map zero? lista))

(funcion-zero?-1 0.0)
(funcion-zero?-2 [1 0x0 0.0 50])
(funcion-zero?-3 '(54 0 65 0.0 18))

(defn funcion-zipmap-1
  [vector vector1]
  (zipmap vector vector1))

(defn funcion-zipmap-2
  [vector vector1]
  (zipmap vector vector1))

(defn funcion-zipmap-3
  [vector vector1]
  (zipmap vector vector1))

(funcion-zipmap-1 [51 98][:a :b])
(funcion-zipmap-2 ["asd" "hola" "bye"]["hola" "como" "estas"])
(funcion-zipmap-3 ["hola" "bye"] [:a :b :c])


